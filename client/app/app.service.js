(function(){
    angular
        .module("EMSApp")
        .service("EMSAppAPI", [
            '$http',
            EMSAppAPI
        ]);
    
    function EMSAppAPI($http){
        var self = this;

        // query string
        self.searchEmployees = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/employees?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        self.getEmployee = function(emp_no){
            console.log(emp_no);
            return $http.get("/api/employees/" + emp_no)
        }

        self.updateEmployee = function(employee){
            console.log(employee);
            return $http.put("/api/employees",employee);
        }

        self.deleteEmployee = function(emp_no){
            console.log(emp_no);
            return $http.delete("/api/employees/"+ emp_no);
        }
        
        // parameterized values
        /*
        self.searchEmployees = function(value){
            return $http.get("/api/employees/" + value);
        }*/

        // post by body over request
        self.addEmployee = function(employee){
            return $http.post("/api/employees", employee);
        }
    }
})();